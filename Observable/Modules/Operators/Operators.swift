//
//  Observable
//
//  Created by Leszek Ślażyński on 21/06/14.
//  Copyright © 2021 Rights reserved by project maintainers.
//

import Foundation

// MARK: Value Read Operators

/// Operator to read the base value from an observable object.
postfix operator ^^

/// Returns the base value from the observable.
///
/// Allows access to the observed base value as `let value = ^^observable`.
/// This operation assumes an immutable observable and is assumed safe during
/// a mutation on the observed base value (e.g. an operation initiated
/// from `willSet` or `didSet` hooks) .
public postfix func ^^<Observable: ObservableProtocol>(observable: Observable) -> Observable.Value {
	return observable.base
}

/// Returns the base value from the observable.
///
/// Allows access to the observed base value as `let value = ^^observable`.
/// This operation is not safe during a mutation (e.g. an operation initiated
/// from `willSet` or `didSet` hooks) on the observed base value and must
/// be deferred to after the write is completed.
public postfix func ^^<Observable: WritableObservableProtocol>(observable: Observable) -> Observable.Value {
	return observable.base
}

// MARK: Value Write Operators

/// Operator to write a new base value into an observable object.
infix operator ^^=

/// Assigns the new given base value into the unownable observable.
///
/// Example: `observable ^= newValue`.
public func ^^=<Observable: WritableObservableProtocol & UnownableObservableProtocol>(observable: inout Observable, value: Observable.Value) {
	observable.base = value
}

/// Assigns the new given base value into the observable.
///
/// Example: `observable ^= newValue`.
public func ^^=<Observable: WritableObservableProtocol>(observable: inout Observable, value: Observable.Value) {
	observable.base = value
}

// MARK: Subscription/Event Operators

/// Adds a new `didChange` event subscription to the observable.
///
/// Example: `observable += { valueChange in … }`.
@discardableResult
public func +=<Observable: ObservableProtocol>(observable: inout Observable, block: @escaping (ValueChange<Observable.Value>) -> ()) -> EventSubscription<ValueChange<Observable.Value>> {
    return observable.didChange += block
}

/// Adds a new `didChange` event subscription to the observable.
///
/// Example: `observable += { oldValue, newValue in … }`.
@discardableResult
public func +=<Observable: ObservableProtocol>(observable: inout Observable, block: @escaping (Observable.Value, Observable.Value) -> ()) -> EventSubscription<ValueChange<Observable.Value>> {
    return observable.didChange += block
}

/// Adds a new `didChange` event subscription to the observable.
///
/// Example: `observable += { newValue in … }`.
@discardableResult
public func +=<Observable: ObservableProtocol>(observable: inout Observable, block: @escaping (Observable.Value) -> ()) -> EventSubscription<ValueChange<Observable.Value>> {
    return observable.didChange += block
}

/// Removes the existing `didChange` event subscription from the observable.
///
/// Example: `observable -= subscription`.
public func -=<Observable: ObservableProtocol>(observable: inout Observable, subscription: EventSubscription<ValueChange<Observable.Value>>) {
    observable.didChange.remove(subscription)
}

/// Adds a new handler block to the event.
///
/// Example: `event += { oldValue, newValue in … }`.
@discardableResult
public func +=<Base>(event: EventReference<ValueChange<Base>>, handler: @escaping (Base, Base) -> ()) -> EventSubscription<ValueChange<Base>> {
    return event.add({ handler($0.oldValue, $0.newValue) })
}

/// Adds a new handler block to the event.
///
/// Example: `event += { newValue in … }`.
@discardableResult
public func +=<Base>(event: EventReference<ValueChange<Base>>, handler: @escaping (Base) -> ()) -> EventSubscription<ValueChange<Base>> {
    return event.add({ valueChange in handler(valueChange.newValue) })
}
