//
//  Observable
//
//  Created by August Saint Freytag on 16/01/2021.
//  Copyright © 2021 Rights reserved by project maintainers.
//

import Foundation

/// An arbitrary event created for changes on an observable base value.
public protocol EventProtocol {
	
	associatedtype ValueType
	
	/// Notify all valid subscriptions of the change. Remove invalid ones.
	mutating func notify(_ base: ValueType)
	
	/// Add an existing subscription.
	@discardableResult
	mutating func add(_ subscription: EventSubscription<ValueType>) -> EventSubscription<ValueType>
	
	/// Create, add and return a subscription for given handler.
	@discardableResult
	mutating func add(_ handler: @escaping (ValueType) -> ()) -> EventSubscription<ValueType>
	
	/// Remove given subscription, if present.
	mutating func remove(_ subscription: EventSubscription<ValueType>)
	
	/// Remove all subscriptions.
	mutating func removeAll()
	
	/// Create, add and return a subscription with given handler and owner.
	@discardableResult
	mutating func add(owner: AnyObject, _ handler: @escaping (ValueType) -> ()) -> EventSubscription<ValueType>
	
}

/// Event which is a value type.
public protocol UnownableEventProtocol: EventProtocol {}

/// Event which is a reference type
public protocol OwnableEventProtocol: EventProtocol {}
