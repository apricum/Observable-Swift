//
//  Observable
//
//  Created by Leszek Ślażyński on 21/06/14.
//  Copyright © 2021 Rights reserved by project maintainers.
//

// Events are implemented as structs, what has both advantages and disadvantages
// Notably they are copied when inside other base value types, and mutated on add/remove/notify
// If you require a reference type for Event, use EventReference<Value> instead

/// A struct representing a collection of subscriptions with means to add, remove and notify them.
public struct Event<Value>: UnownableEventProtocol {
	
    public typealias ValueType = Value
    public typealias SubscriptionType = EventSubscription<Value>
    public typealias HandlerType = SubscriptionType.HandlerType
    
    public private(set) var subscriptions = [SubscriptionType]()
    
    public init() {}
    
    public mutating func notify(_ base: Value) {
        subscriptions = subscriptions.filter { $0.valid() }
        for subscription in subscriptions {
            subscription.handler(base)
        }
    }
    
    @discardableResult
    public mutating func add(_ subscription: SubscriptionType) -> SubscriptionType {
        subscriptions.append(subscription)
        return subscription
    }
    
    @discardableResult
    public mutating func add(_ handler: @escaping HandlerType) -> SubscriptionType {
        return add(SubscriptionType(owner: nil, handler: handler))
    }
    
    public mutating func remove(_ subscription: SubscriptionType) {
        var newSubscriptions = [SubscriptionType]()
        var isFirstSubscription = true
		
        for existing in subscriptions {
            if isFirstSubscription && existing === subscription {
                isFirstSubscription = false
            } else {
                newSubscriptions.append(existing)
            }
        }
		
        subscriptions = newSubscriptions
    }
    
    public mutating func removeAll() {
        subscriptions.removeAll()
    }
    
    @discardableResult
    public mutating func add(owner: AnyObject, _ handler: @escaping HandlerType) -> SubscriptionType {
        return add(SubscriptionType(owner: owner, handler: handler))
    }
    
    public mutating func unshare() {
//        _subscriptions.unshare()
    }
    
}

@discardableResult
public func += <Event: UnownableEventProtocol> (event: inout Event, handler: @escaping (Event.ValueType) -> ()) -> EventSubscription<Event.ValueType> {
    return event.add(handler)
}

@discardableResult
public func += <Event: OwnableEventProtocol> (event: Event, handler: @escaping (Event.ValueType) -> ()) -> EventSubscription<Event.ValueType> {
    var e = event
    return e.add(handler)
}

public func -= <Event: UnownableEventProtocol> (event: inout Event, subscription: EventSubscription<Event.ValueType>) {
    return event.remove(subscription)
}

public func -= <Event: OwnableEventProtocol> (event: Event, subscription: EventSubscription<Event.ValueType>) {
    var mutableEvent = event
    return mutableEvent.remove(subscription)
}
