//
//  Observable
//
//  Created by Leszek Ślażyński on 21/06/14.
//  Copyright © 2021 Rights reserved by project maintainers.
//

// Implemented as a class, so it can be compared using === and !==.

// There is no way for event to get notified when the owner was deallocated,
// therefore it will be invalidated only upon next attempt to trigger.

// Event subscriptions are neither freed nor removed from events upon invalidation.
// Events remove invalidated subscriptions themselves when firing.

// Invalidation immediately frees handler and owned objects.

/// A class representing a subscription for `Event<Value>`.
public class EventSubscription<Value> {
    
    public typealias HandlerType = (Value) -> ()
    
    private var isValid: () -> Bool
    
    /// Handler to be called when value changes.
    public private(set) var handler: HandlerType
    
    /// array of owned objects
    private var ownedObjects = [AnyObject]()
    
    /// When invalid subscription is to be notified, it is removed instead.
    public func valid() -> Bool {
        if !isValid() {
            invalidate()
            return false
        } else {
            return true
        }
    }
    
    /// Marks the event for removal, frees the handler and owned objects
    public func invalidate() {
        isValid = { false }
        handler = { _ in () }
        ownedObjects = []
    }
    
    /// Init with a handler and an optional owner.
    /// If owner is present, valid() is tied to its lifetime.
    public init(owner: AnyObject?, handler: @escaping HandlerType) {
        if owner == nil {
			self.isValid = { true }
        } else {
			self.isValid = { [weak owner] in owner != nil }
        }
		
		self.handler = handler
    }
    
    /// Add an object to be owned while the event is not invalidated
    public func addOwnedObject(_ object: AnyObject) {
        ownedObjects.append(object)
    }
    
    /// Remove object from owned objects
    public func removeOwnedObject(_ object: AnyObject) {
        ownedObjects = ownedObjects.filter{ $0 !== object }
    }
}
