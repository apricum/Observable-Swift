//
//  Observable
//
//  Created by Leszek Ślażyński on 28/06/14.
//  Copyright © 2021 Rights reserved by project maintainers.
//

/// A subclass of event reference allowing it to own other object[s].
/// Additionally, the reference makes added events own itself.
/// This retain cycle allows owned objects to live as long as valid subscriptions exist.
public class OwningEventReference<Value>: EventReference<Value> {
    
    var ownedObject: AnyObject? = nil

    public override func add(_ subscription: SubscriptionType) -> SubscriptionType {
        let addedSubscription = super.add(subscription)
		
        if ownedObject != nil {
            addedSubscription.addOwnedObject(self)
        }
		
        return addedSubscription
    }
    
    public override func add(_ handler: @escaping (Value) -> ()) -> EventSubscription<Value> {
        let addedSubscription = super.add(handler)
		
        if ownedObject != nil {
            addedSubscription.addOwnedObject(self)
        }
		
        return addedSubscription
    }
    
    public override func remove(_ subscription: SubscriptionType) {
        subscription.removeOwnedObject(self)
        super.remove(subscription)
    }
    
    public override func removeAll() {
        for subscription in event.subscriptions {
            subscription.removeOwnedObject(self)
        }
		
        super.removeAll()
    }
    
    public override func add(owner: AnyObject, _ handler:  @escaping HandlerType) -> SubscriptionType {
        let addedSubscription = super.add(owner: owner, handler)
		
        if ownedObject != nil {
            addedSubscription.addOwnedObject(self)
        }
		
        return addedSubscription
    }

    public override init(event: Event<Value>) {
        super.init(event: event)
    }
    
}
