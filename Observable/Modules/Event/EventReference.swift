//
//  Observable
//
//  Created by Leszek Ślażyński on 23/06/14.
//  Copyright © 2021 Rights reserved by project maintainers.
//

/// A class enclosing an Event struct, exposed and usable as a reference type.
open class EventReference<Value>: OwnableEventProtocol {
	
    public typealias ValueType = Value
    public typealias SubscriptionType = EventSubscription<Value>
    public typealias HandlerType = EventSubscription<Value>.HandlerType
    
    public private(set) var event: Event<Value>
	
	public var numberOfSubscriptions: Int { event.subscriptions.count }
	
	public var hasSubscriptions: Bool { event.subscriptions.isEmpty == false }
    
    open func notify(_ base: Value) {
        event.notify(base)
    }
    
    @discardableResult
    open func add(_ subscription: SubscriptionType) -> SubscriptionType {
        return event.add(subscription)
    }
    
    @discardableResult
    open func add(_ handler: @escaping (Value) -> ()) -> EventSubscription<Value> {
        return event.add(handler)
    }
    
    open func remove(_ subscription: SubscriptionType) {
        return event.remove(subscription)
    }
    
    open func removeAll() {
        event.removeAll()
    }
    
    @discardableResult
    open func add(owner: AnyObject, _ handler: @escaping HandlerType) -> SubscriptionType {
        return event.add(owner: owner, handler)
    }
    
    public convenience init() {
        self.init(event: Event<Value>())
    }
    
    public init(event: Event<Value>) {
        self.event = event
    }
    
}
