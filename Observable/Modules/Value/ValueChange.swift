//
//  Observable
//
//  Created by August Saint Freytag on 17/01/2021.
//  Copyright © 2021 Rights reserved by project maintainers.
//

/// A struct representing information associated with a value change event.
public struct ValueChange<Value> {
	
	public let oldValue: Value
	public let newValue: Value
	
	public init(_ oldValue: Value, _ newValue: Value) {
		self.oldValue = oldValue
		self.newValue = newValue
	}
	
}
