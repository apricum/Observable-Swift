//
//  Observable
//
//  Created by Leszek Ślażyński on 20/06/14.
//  Copyright © 2021 Rights reserved by project maintainers.
//

public class PairObservable<FirstObservable: ObservableProtocol, SecondObservable: ObservableProtocol>: OwnableObservableProtocol {
    
	public typealias FirstBaseType = FirstObservable.Value
    public typealias SecondBaseType = SecondObservable.Value
    
    public typealias BaseType = (FirstBaseType, SecondBaseType)
    
    public private(set) var willChange = EventReference<ValueChange<(FirstBaseType, SecondBaseType)>>()
    public private(set) var didChange = EventReference<ValueChange<(FirstBaseType, SecondBaseType)>>()
	public private(set) var didDispatch = EventReference<ValueChange<(FirstBaseType, SecondBaseType)>>()
    
    var firstBase: FirstBaseType
    var secondBase: SecondBaseType
    
    public var base: (FirstBaseType, SecondBaseType) { (firstBase, secondBase) }
    
    private let firstObservable: FirstObservable
    private let secondObservable: SecondObservable
    
    public init (_ firstObservable: FirstObservable, _ secondObservable: SecondObservable) {
		self.firstObservable = firstObservable
		self.secondObservable = secondObservable
		self.firstBase = firstObservable.base
		self.secondBase = secondObservable.base
		
        firstObservable.willChange.add(owner: self) { [weak self] change in
            let oldValue = (change.oldValue, self!.secondBase)
            let newValue = (change.newValue, self!.secondBase)
            let change = ValueChange(oldValue, newValue)
			
            self!.willChange.notify(change)
        }
		
        firstObservable.didChange.add(owner: self) { [weak self] change in
            let newValueInChange = change.newValue
            self!.firstBase = newValueInChange
			
            let oldValueInSecond = (change.oldValue, self!.secondBase)
            let newValueInSecond = (change.newValue, self!.secondBase)
            let change = ValueChange(oldValueInSecond, newValueInSecond)
			
            self!.didChange.notify(change)
        }
		
		firstObservable.didDispatch.add(owner: self) { [weak self] change in
			let oldValueInSecond = (change.oldValue, self!.secondBase)
			let newValueInSecond = (change.newValue, self!.secondBase)
			let change = ValueChange(oldValueInSecond, newValueInSecond)
			
			self!.didDispatch.notify(change)
		}
		
        secondObservable.willChange.add(owner: self) { [weak self] change in
            let oldValueInFirst = (self!.firstBase, change.oldValue)
            let newValueInFirst = (self!.firstBase, change.newValue)
            let change = ValueChange(oldValueInFirst, newValueInFirst)
			
            self!.willChange.notify(change)
        }
		
        secondObservable.didChange.add(owner: self) { [weak self] change in
            let newValueInChange = change.newValue
            self!.secondBase = newValueInChange
			
            let oldValueInFirst = (self!.firstBase, change.oldValue)
            let newValueInFirst = (self!.firstBase, change.newValue)
            let change = ValueChange(oldValueInFirst, newValueInFirst)
			
            self!.didChange.notify(change)
        }
		
		secondObservable.didDispatch.add(owner: self) { [weak self] change in
			let oldValueInFirst = (self!.firstBase, change.oldValue)
			let newValueInFirst = (self!.firstBase, change.newValue)
			let change = ValueChange(oldValueInFirst, newValueInFirst)
			
			self!.didDispatch.notify(change)
		}
    }

}

public func & <FirstObservable: ObservableProtocol, SecondObservable: ObservableProtocol>(x: FirstObservable, y: SecondObservable) -> PairObservable<FirstObservable, SecondObservable> {
    return PairObservable(x, y)
}
