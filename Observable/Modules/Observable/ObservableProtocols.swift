//
//  Observable
//
//  Created by August Saint Freytag on 16/01/2021.
//  Copyright © 2021 Rights reserved by project maintainers.
//

import Foundation

/// An arbitrary observable.
public protocol ObservableProtocol {
	
	associatedtype Value
	
	/// The base value of the observable.
	var base: Value { get }
	
	/// The value event fired before the base value is changed.
	var willChange: EventReference<ValueChange<Value>> { get }
	
	/// The value event fired after the base value is changed.
	var didChange: EventReference<ValueChange<Value>> { get }
	
	/// The value event fired after the base value is changed and
	/// accessors to the struct has been dispatched.
	var didDispatch: EventReference<ValueChange<Value>> { get }
}

/// An observable that can be written to.
public protocol WritableObservableProtocol: ObservableProtocol {
	
	/// The base value of the observable.
	var base: Value { get set }
	
}

/// Observable that is a value type. Elementary observables are value types.
public protocol UnownableObservableProtocol: WritableObservableProtocol {
	
	/// Unshares all events.
	mutating func unshare(removeSubscriptions: Bool)
	
}

/// Observable that is a reference type. Compound observables are reference types.
public protocol OwnableObservableProtocol: ObservableProtocol {}
