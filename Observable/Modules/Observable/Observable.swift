//
//  Observable
//
//  Created by Leszek Ślażyński on 20/06/14.
//  Copyright © 2021 Rights reserved by project maintainers.
//

/// A struct representing an observable value.
///
/// Shares value and mutability semantics when used with an underlying value type.
public struct Observable<Value>: UnownableObservableProtocol {	
    
    public typealias ValueType = Value
	public typealias ChangeEventReference = EventReference<ValueChange<Value>>

    public private(set) var willChange = ChangeEventReference()
    public private(set) var didChange = ChangeEventReference()
	public private(set) var didDispatch = ChangeEventReference()
	
	/// The base value of the observable.
    public var base: Value {
		willSet {
			willChange.notify(ValueChange(base, newValue))
		}
		didSet {
			didChange.notify(ValueChange(oldValue, base))
			
			if didDispatch.hasSubscriptions {
				notifyDispatch(ValueChange(oldValue, base))
			}
		}
    }
	
	private func notifyDispatch(_ change: ValueChange<Value>) {
		DispatchQueue.main.async(qos: .userInteractive) {
			self.didDispatch.notify(change)
		}
	}
    
    public mutating func unshare(removeSubscriptions: Bool) {
        if removeSubscriptions {
            willChange = ChangeEventReference()
            didChange = ChangeEventReference()
        } else {
            var beforeChangeEvent = willChange.event
            beforeChangeEvent.unshare()
            willChange = ChangeEventReference(event: beforeChangeEvent)
			
            var afterChangeEvent = didChange.event
            afterChangeEvent.unshare()
            didChange = ChangeEventReference(event: afterChangeEvent)
        }
    }

    public init(_ value: Value) {
		self.base = value
    }
	
}
