//
//  Observable
//
//  Created by Leszek Ślażyński on 23/06/14.
//  Copyright © 2021 Rights reserved by project maintainers.
//

public class ObservableChainingProxy<BaseObservable: ObservableProtocol, TargetObservable: ObservableProtocol>: OwnableObservableProtocol {
    
    public typealias Value = TargetObservable.Value?
	public typealias ChangeEventReference = EventReference<ValueChange<Value>>
	public typealias OwningChangeEventReference = OwningEventReference<ValueChange<Value>>
	
    public var base: Value { nil }
    
    private weak var willChangeReference: ChangeEventReference? = nil
    private weak var didChangeReference: ChangeEventReference? = nil
	private weak var didDispatchReference: ChangeEventReference? = nil
    
    public var willChange: ChangeEventReference {
        return unwrappedChangeEventReference(at: \.willChangeReference)
    }
    
    public var didChange: ChangeEventReference {
		return unwrappedChangeEventReference(at: \.didChangeReference)
    }
	
	public var didDispatch: ChangeEventReference {
		return unwrappedChangeEventReference(at: \.didDispatchReference)
	}
	
	private func unwrappedChangeEventReference(at keyPath: KeyPath<ObservableChainingProxy<BaseObservable, TargetObservable>, ChangeEventReference?>) -> ChangeEventReference {
		guard let event = self[keyPath: keyPath] else {
			let event = OwningChangeEventReference()
			
			event.ownedObject = self
			didChangeReference = event
			
			return event
		}
		
		return event
	}
    
    private let root: BaseObservable
    private let path: (BaseObservable.Value) -> TargetObservable?
    
    private func targetChangeToValueChange(_ valueChange: ValueChange<TargetObservable.Value>) -> ValueChange<Value> {
        let oldValue = Optional.some(valueChange.oldValue)
        let newValue = Optional.some(valueChange.newValue)
		
        return ValueChange(oldValue, newValue)
    }
        
    private func objectChangeToValueChange(_ objectChange: ValueChange<BaseObservable.Value>) -> ValueChange<Value> {
        let oldValue = path(objectChange.oldValue)?.base
        let newValue = path(objectChange.newValue)?.base
		
        return ValueChange(oldValue, newValue)
    }
    
    init(base: BaseObservable, path: @escaping (BaseObservable.Value) -> TargetObservable?) {
        self.root = base
        self.path = path

        let willChangeSubscription = EventSubscription(owner: self) { [weak self] in
            self!.willChange.notify(self!.targetChangeToValueChange($0))
        }
        
        let didChangeSubscription = EventSubscription(owner: self) { [weak self] in
            self!.didChange.notify(self!.targetChangeToValueChange($0))
        }
		
		let didDispatchSubscription = EventSubscription(owner: self) { [weak self] in
			self!.didDispatch.notify(self!.targetChangeToValueChange($0))
		}
        
        base.willChange.add(owner: self) { [weak self] objectChange in
            let oldTarget = path(objectChange.oldValue)
            oldTarget?.willChange.remove(willChangeSubscription)
            oldTarget?.didChange.remove(didChangeSubscription)
			oldTarget?.didDispatch.remove(didDispatchSubscription)
			
			let valueChange = self!.objectChangeToValueChange(objectChange)
            self!.willChange.notify(valueChange)
        }
        
        base.didChange.add(owner: self) { [weak self] objectChange in
			let valueChange = self!.objectChangeToValueChange(objectChange)
            self!.didChange.notify(valueChange)
        }
		
		base.didDispatch.add(owner: self) { [weak self] objectChange in
			let valueChange = self!.objectChangeToValueChange(objectChange)
			self!.didDispatch.notify(valueChange)
			
			let newTarget = path(objectChange.newValue)
			newTarget?.willChange.add(willChangeSubscription)
			newTarget?.didChange.add(didChangeSubscription)
			newTarget?.didDispatch.add(didDispatchSubscription)
		}
    }
    
    public func to<FarTargetObservable: ObservableProtocol>(path: @escaping (TargetObservable.Value) -> FarTargetObservable?) -> ObservableChainingProxy<ObservableChainingProxy<BaseObservable, TargetObservable>, FarTargetObservable> {
        func cascadeNil(_ base: Value) -> FarTargetObservable? {
            if let base = base {
                return path(base)
            } else {
                return nil
            }
        }
		
        return ObservableChainingProxy<ObservableChainingProxy<BaseObservable, TargetObservable>, FarTargetObservable>(base: self, path: cascadeNil)
    }
    
    public func to<FarTargetObservable: ObservableProtocol>(path: @escaping (TargetObservable.Value) -> FarTargetObservable) -> ObservableChainingProxy<ObservableChainingProxy<BaseObservable, TargetObservable>, FarTargetObservable> {
        func cascadeNil(_ base: Value) -> FarTargetObservable? {
            if let base = base {
                return path(base)
            } else {
                return nil
            }
        }
		
        return ObservableChainingProxy<ObservableChainingProxy<BaseObservable, TargetObservable>, FarTargetObservable>(base: self, path: cascadeNil)
    }
    
}

public struct ObservableChainingBase<BaseObservable: ObservableProtocol> {
    
	fileprivate let base: BaseObservable
	
    public func to<TargetObservable: ObservableProtocol>(_ path: @escaping (BaseObservable.Value) -> TargetObservable?) -> ObservableChainingProxy<BaseObservable, TargetObservable> {
        return ObservableChainingProxy(base: base, path: path)
    }
	
    public func to<TargetObservable: ObservableProtocol>(_ path: @escaping (BaseObservable.Value) -> TargetObservable) -> ObservableChainingProxy<BaseObservable, TargetObservable> {
        return ObservableChainingProxy(base: base, path: { value in .some(path(value)) })
    }
	
}

public func chain<Observable: ObservableProtocol>(_ observable: Observable) -> ObservableChainingBase<Observable> {
    return ObservableChainingBase(base: observable)
}

public func / <BaseObservable: ObservableProtocol, TargetObservable: ObservableProtocol, FarTargetObservable: ObservableProtocol> (observable: ObservableChainingProxy<BaseObservable, TargetObservable>, path: @escaping (TargetObservable.Value) -> FarTargetObservable?) -> ObservableChainingProxy<ObservableChainingProxy<BaseObservable, TargetObservable>, FarTargetObservable> {
    return observable.to(path: path)
}

public func / <BaseObservable: ObservableProtocol, TargetObservable: ObservableProtocol> (observable: BaseObservable, path: @escaping (BaseObservable.Value) -> TargetObservable?) -> ObservableChainingProxy<BaseObservable, TargetObservable> {
    return ObservableChainingProxy(base: observable, path: path)
}
