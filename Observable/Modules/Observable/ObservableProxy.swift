//
//  Observable
//
//  Created by Leszek Ślażyński on 24/06/14.
//  Copyright © 2021 Rights reserved by project maintainers.
//

// two generic parameters are needed to be able to override `value` in `ObservableReference<T>`

open class ObservableProxy<Base, Observable: ObservableProtocol>: OwnableObservableProtocol where Observable.Value == Base {
    
    public typealias BaseType = Base
    
    public private(set) var willChange = EventReference<ValueChange<Base>>()
    public private(set) var didChange = EventReference<ValueChange<Base>>()
	public private(set) var didDispatch = EventReference<ValueChange<Base>>()
    
    /// Private storage for base value, used in case subclasses override value with a setter.
    private var storedBase: Base
    
    open var base: Base { storedBase }
    
    public init(_ observable: Observable) {
        self.storedBase = observable.base
		
        observable.willChange.add(owner: self) { [weak self] change in
            self!.willChange.notify(change)
        }
		
        observable.didChange.add(owner: self) { [weak self] change in
            let newValue = change.newValue
			
            self!.storedBase = newValue
            self!.didChange.notify(change)
        }
		
		observable.didDispatch.add(owner: self) { [weak self] change in
			self!.didDispatch.notify(change)
		}
    }
    
}

public func proxy <Observable: ObservableProtocol> (_ observable: Observable) -> ObservableProxy<Observable.Value, Observable> {
    return ObservableProxy(observable)
}
