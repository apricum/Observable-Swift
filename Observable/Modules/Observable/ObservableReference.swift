//
//  Observable
//
//  Created by Leszek Ślażyński on 21/06/14.
//  Copyright © 2021 Rights reserved by project maintainers.
//

/// A reference wrapper around an observable value.
public class ObservableReference<Value>: ObservableProxy<Value, Observable<Value>>, WritableObservableProtocol {
    
    public typealias ValueType = Value
    
    private var storedObservable: Observable<Value>
    
    public override var base: Value {
        get { storedObservable.base }
        set { storedObservable.base = newValue }
    }
    
    public init(_ base: Value) {
        storedObservable = Observable(base)
        super.init(storedObservable)
    }
    
}
